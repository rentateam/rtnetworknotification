# RTNetworkNotification

[![CI Status](http://img.shields.io/travis/a-25/RTNetworkNotification.svg?style=flat)](https://travis-ci.org/a-25/RTNetworkNotification)
[![Version](https://img.shields.io/cocoapods/v/RTNetworkNotification.svg?style=flat)](http://cocoapods.org/pods/RTNetworkNotification)
[![License](https://img.shields.io/cocoapods/l/RTNetworkNotification.svg?style=flat)](http://cocoapods.org/pods/RTNetworkNotification)
[![Platform](https://img.shields.io/cocoapods/p/RTNetworkNotification.svg?style=flat)](http://cocoapods.org/pods/RTNetworkNotification)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

RTNetworkNotification is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "RTNetworkNotification"
```

## Author

a-25, a-25@rentateam.ru

## License

RTNetworkNotification is available under the MIT license. See the LICENSE file for more info.
