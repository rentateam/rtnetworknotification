//
//  RTNetworkNotificationShower.h
//
//  Created by A-25 on 14/12/15.
//  Copyright © 2015 rentateam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <AFNetworking/AFNetworking.h>

@interface RTNetworkNotificationShower : NSObject
{
    BOOL isNotificationVisible;
    UIView *notificationEmbedView;
    CGPoint notificationEmbedOrigin;
}

@property(nonatomic,strong) UIView *notificationView;
@property(nonatomic,strong) UIWindow *applicationWindow;
@property(nonatomic,strong) AFNetworkReachabilityManager *reachabilityManager;

-(void)activate;
-(void)deactivate;
-(void)refreshNetworkView;
-(void)refreshNetworkView:(UIView*)viewToEmbed;
-(void)refreshNetworkView:(UIView*)viewToEmbed withOrigin:(CGPoint)origin;

@end
