//
//  RTNetworkNotificationControllerProtocol.h
//
//  Created by A-25 on 14/12/15.
//  Copyright © 2015 rentateam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol RTNetworkNotificationControllerProtocol <NSObject>

@optional
-(UIView*)getViewToEmbedNotification;

@end
