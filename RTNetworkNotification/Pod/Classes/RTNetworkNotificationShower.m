//
//  RTNetworkNotificationShower.m
//
//  Created by A-25 on 14/12/15.
//  Copyright © 2015 rentateam. All rights reserved.
//

#import "RTNetworkNotificationShower.h"
#import "RTNetworkNotificationControllerProtocol.h"

@implementation RTNetworkNotificationShower

-(id)init
{
    self = [super init];
    if(self != nil){
        isNotificationVisible = NO;
        notificationEmbedOrigin = CGPointZero;
    }
    return self;
}

-(void)activate
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onNetworkReachabilityChanged:) name:AFNetworkingReachabilityDidChangeNotification object:nil];
    [self.reachabilityManager startMonitoring];
}

-(void)deactivate
{
    [self.reachabilityManager stopMonitoring];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AFNetworkingReachabilityDidChangeNotification object:nil];
}

-(void)dealloc
{
    [self deactivate];
}

-(void)refreshNetworkView
{
    [self removeNotificationFromInactiveView];
    [self displayNetworkStatus:self.reachabilityManager.networkReachabilityStatus inView:[self getViewToEmbed] withOrigin:CGPointZero];
}

-(void)refreshNetworkView:(UIView*)viewToEmbed
{
    [self removeNotificationFromInactiveView];
    [self displayNetworkStatus:self.reachabilityManager.networkReachabilityStatus inView:viewToEmbed withOrigin:CGPointZero];
}

-(void)refreshNetworkView:(UIView*)viewToEmbed withOrigin:(CGPoint)origin
{
    [self removeNotificationFromInactiveView];
    [self displayNetworkStatus:self.reachabilityManager.networkReachabilityStatus inView:viewToEmbed withOrigin:origin];
}

-(void)removeNotificationFromInactiveView
{
    if(isNotificationVisible){
        [self.notificationView removeFromSuperview];
        isNotificationVisible = NO;
    }
}

-(void)onNetworkReachabilityChanged:(NSNotification*)notification
{
    AFNetworkReachabilityStatus status = [[notification userInfo][AFNetworkingReachabilityNotificationStatusItem] intValue];
    [self displayNetworkStatus:status inView:notificationEmbedView withOrigin:notificationEmbedOrigin];
}

-(void)displayNetworkStatus:(AFNetworkReachabilityStatus)status inView:(UIView*)embedView withOrigin:(CGPoint)origin
{
    notificationEmbedView = embedView;
    notificationEmbedOrigin = origin;
    switch(status){
        case AFNetworkReachabilityStatusNotReachable:
            {
                [self networkIsUnreachable];
            }
            break;
        case AFNetworkReachabilityStatusReachableViaWWAN:
        case AFNetworkReachabilityStatusReachableViaWiFi:
            {
                [self networkIsReachable];
            }
            break;
        default:
            {
                return;
            }
            break;
    }
}

-(void)networkIsReachable
{
    if(!isNotificationVisible)
        return;
    [self.notificationView removeFromSuperview];
    isNotificationVisible = NO;
}

-(void)networkIsUnreachable
{
    if(isNotificationVisible)
        return;
    [notificationEmbedView addSubview:self.notificationView];
    self.notificationView.frame = CGRectMake(notificationEmbedOrigin.x, notificationEmbedOrigin.y, self.notificationView.frame.size.width, self.notificationView.frame.size.height);
    [self.notificationView setNeedsLayout];
    isNotificationVisible = YES;
}

-(UIView*)getViewToEmbed
{
    UIViewController *rootController = self.applicationWindow.rootViewController;
    UIView *result = [self getViewToEmbedForNotificationProtocol:rootController];
    if(result != nil){
        return result;
    }
    
    if([rootController isKindOfClass:[UINavigationController class]]){
        UIView *result = [self getViewToEmbedForNavigationController:((UINavigationController*)rootController)];
        if(result != nil){
            return result;
        }
    }
    
    if([rootController isKindOfClass:[UITabBarController class]]){
        UIViewController *selectedViewController = [((UITabBarController*)rootController) selectedViewController];
        if([selectedViewController isKindOfClass:[UINavigationController class]]){
            UIView *result = [self getViewToEmbedForNavigationController:((UINavigationController*)selectedViewController)];
            if(result != nil){
                return result;
            }
        }
        UIView *result = [self getViewToEmbedForNotificationProtocol:selectedViewController];
        if(result != nil){
            return result;
        }
    }
    
    return rootController.view;
}

-(UIView*)getViewToEmbedForNotificationProtocol:(UIViewController*)controller
{
    if([controller conformsToProtocol:@protocol(RTNetworkNotificationControllerProtocol)]){
        return [((UIViewController<RTNetworkNotificationControllerProtocol>*)controller) getViewToEmbedNotification];
    }
    return nil;
}

-(UIView*)getViewToEmbedForNavigationController:(UINavigationController*)controller
{
    UIViewController *topController = [controller topViewController];
    UIView *result = [self getViewToEmbedForNotificationProtocol:topController];
    if(result != nil){
        return result;
    }
    return topController.view;
}

@end
