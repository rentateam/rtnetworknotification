#
# Be sure to run `pod lib lint RTNetworkNotification.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "RTNetworkNotification"
  s.version          = “1.0.0”
  s.summary          = “Universal showing of notification when network is unreachable.”

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!  
  s.description      = <<-DESC
A library that shows a notification when network is unreachable.
                       DESC

  s.homepage         = "https://bitbucket.org/rentateam/rtnetworknotification"
  # s.screenshots     = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = 'MIT'
  s.author           = { "a-25" => "a-25@rentateam.ru" }
  s.source           = { :git => "hhttps://bitbucket.org/rentateam/rtnetworknotification", :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.platform     = :ios, ‘8.0’
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'
  s.resource_bundles = {
    'RTNetworkNotification' => ['RTNetworkNotification/Pod/Assets/*.png']
  }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  s.frameworks = 'UIKit'
  s.dependency 'AFNetworking', '~> 2.6.0'
end
